# shioyaki

Yuji Yamamoto's personal file collection in SQLite.

## How to build

### Clone HRR and related repositories

TODO: Use git-submodule

```bash
cd vendor
git clone https://github.com/khibino/haskell-relational-record.git
git clone https://github.com/khibino/haskell-product-isomorphic.git
```

### Prerequisites

#### Windows

- SQLite 3
    1. Download the zip files below from <http://www.sqlite.org/download.html>:
        - `sqlite-amalgamation-XXXX.zip`
        - `sqlite-dll-win64-x64-XXXX.zip`
        - `sqlite-tools-win32-x86-XXXX.zip`
        - Where `XXXX` is replaced with the version number. Currently tested with `3220000`.
    1. Unzip and copy files in the zip files to `C:\lib\sqlite`.
    1. Add `C:\lib\sqlite` to the `PATH` environment variable.
