#!/bin/bash

stack clean
stack test --fast --haddock-deps --file-watch --ghc-options=-j --ghc-options=-ddump-splices
