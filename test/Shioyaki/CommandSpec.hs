module Shioyaki.CommandSpec
  ( spec
  , main
  ) where


import           Control.Monad (forM)
import qualified Data.Text as T
import           Data.Time.Calendar (fromGregorian)
import           Data.Time.Clock (UTCTime(UTCTime), picosecondsToDiffTime)
import           System.Directory (createDirectory, setModificationTime)
import qualified System.Environment as Env
import           System.Exit (ExitCode(ExitFailure))
import           System.FilePath ((</>))
import           System.IO.Temp (withTempDirectory)
import           Test.Hspec

import qualified Shioyaki.Command as Shioyaki
import qualified Shioyaki.Entity as Shioyaki
import qualified Shioyaki.Item as Shioyaki
import qualified Shioyaki.Monad as Shioyaki
import qualified Shioyaki.Util.Db as Shioyaki


main :: IO ()
main = hspec spec


spec :: Spec
spec =
  describe "Shioyaki.Command.main" $ do
    let commonTargetFiles =
          [ ("1' or '1' = '1';-- .txt", mkUtcTime 2018 3 1)
          , ("2' or '2' = '2';-- .txt", mkUtcTime 2018 3 2)
          ]
        commonRecords =
          [ Shioyaki.Item "1' or '1' = '1';-- .txt" "2018/03/01/00-00-00" ".txt" []
          , Shioyaki.Item "2' or '2' = '2';-- .txt" "2018/03/02/00-00-00" ".txt" []
          ]
        commonCsTags = "tag1',tag2' or '2'"

    describe "register subcommand" $
      context "Given an empty database" $
        it "registers the given files to index" $
          withEmptyDbAndNonRegisteredFiles commonTargetFiles $ \dbPath filesToRegister -> do
            runShioyakiWithDb dbPath ("register" : filesToRegister)
            listAllItems dbPath `shouldReturn` commonRecords

    describe "tag subcommand" $ do
      context "Given an empty database, and the specified items are not registered" $
        it "exits with an error" $
          withEmptyDb $ \dbPath -> do
            runShioyakiWithDb dbPath ("tag" : commonCsTags : ["non-exisiting.txt"])
              `shouldThrow` anyExitFailure
            listAllItems dbPath `shouldReturn` []

      context "After adding some items" $
        it "adds tags to the specified items" $
          withEmptyDbAndNonRegisteredFiles commonTargetFiles $ \dbPath filesToRegister -> do
            runShioyakiWithDb dbPath ("register" : filesToRegister)

            let itemToTag1 = commonRecords !! 0
                itemToTag2 = commonRecords !! 1

            runShioyakiWithDb dbPath $
              "tag" : commonCsTags : [T.unpack (Shioyaki.itemOriginalName itemToTag1)]

            let moreTag = "moreTag \\  ' or '0' = '0'\\"
            runShioyakiWithDb dbPath $
              "tag" : moreTag : [T.unpack (Shioyaki.itemRegisteredName itemToTag2)]

            listAllItems dbPath `shouldReturn`
              [ Shioyaki.replaceTags (T.splitOn "," $ T.pack commonCsTags) itemToTag1
              , Shioyaki.replaceTags [T.pack moreTag] itemToTag2
              ]


-- Utility function for testing
withEmptyDbAndTmpPath :: (FilePath -> FilePath -> IO a) -> IO a
withEmptyDbAndTmpPath action =
  withTempDirectory "tmp" "test" $ \tmpPath -> do
    let dbRoot = tmpPath </> "db"
    createDirectory dbRoot
    action dbRoot tmpPath


-- Utility function for testing
withEmptyDb :: (FilePath -> IO a) -> IO a
withEmptyDb action =
  withEmptyDbAndTmpPath $ \dbRoot _tmpPath -> action dbRoot


-- Utility function for testing
withEmptyDbAndNonRegisteredFiles
  :: [(FilePath, UTCTime)] -> (FilePath -> [FilePath] -> IO a) -> IO a
withEmptyDbAndNonRegisteredFiles pathAndMTimes action =
  withEmptyDbAndTmpPath $ \dbRoot tmpPath -> do
    paths <- forM pathAndMTimes $ \(relPath, time) -> do
      let path = tmpPath </> relPath
      writeFile path ""
      setModificationTime path time
      return path
    action dbRoot paths


-- Utility function for testing
mkUtcTime :: Integer -> Int -> Int -> UTCTime
mkUtcTime y m d =
  UTCTime (fromGregorian y m d) (picosecondsToDiffTime 0)


-- Utility function for testing
anyExitFailure :: ExitCode -> Bool
anyExitFailure (ExitFailure _) = True
anyExitFailure _ = False


runShioyakiWithDb :: FilePath -> [FilePath] -> IO ()
runShioyakiWithDb dbPath args =
  Env.withArgs ("--db" : dbPath : args) Shioyaki.main


listAllItems :: FilePath -> IO [Shioyaki.Item]
listAllItems dbPath =
  Shioyaki.runShioyakiM
    (Shioyaki.envIoOfOptions (Shioyaki.GlobalOptions dbPath undefined))
    (Shioyaki.withConnectionCommit Shioyaki.listAll)
