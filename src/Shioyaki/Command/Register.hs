module Shioyaki.Command.Register
  ( main
  ) where

import qualified Shioyaki.Item
import           Shioyaki.Monad


main :: [FilePath] -> ShioyakiM ()
main = Shioyaki.Item.registerFiles
