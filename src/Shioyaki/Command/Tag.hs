module Shioyaki.Command.Tag
  ( main
  ) where

import           Control.Monad (when)
import           Data.String (fromString)

import           Shioyaki.Error
import qualified Shioyaki.Item
import           Shioyaki.Monad
import qualified Shioyaki.Util.Db as Db
import           Shioyaki.Value ()


-- TODO: Which module should be responsible to handle an invalid inputs?
-- TODO: Parse spaces around a comma.
-- TODO: Reject other invalid characters.
-- TODO: Trim surrounding spaces.
-- TODO: --replace option
main
  :: String
  -> [String]
  -> ShioyakiM ()
main "" [] = die "Enter comma separated tag names and items' original name or registered name!"
main "" _  = die "Tag name must not be empty!"
main _  [] = die "Item names must not be empty!"
main csTags itemNames =
  Db.withConnectionCommit $ \c -> do
    is <- Shioyaki.Item.findByNames c itemNames
    when (null is) $
      die $ "No items matched with any name of " ++ show itemNames ++ "."
    Shioyaki.Item.updateTags c (fromString csTags) is
