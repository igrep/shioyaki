module Shioyaki.Item.Dao
  ( insertMany
  , selectAll
  , selectByNames
  , selectBy
  ) where


import           Control.Monad (void)
import           Data.Functor.ProductIsomorphic ((|$|), (|*|))
import           Database.HDBC (ConnWrapper)
import           Database.Relational
                   ( Record
                   , Flat
                   , Relation
                   , QueryAggregate
                   , aggregateRelation
                   , groupBy
                   , in'
                   , just
                   , on
                   , or'
                   , query
                   , queryMaybe
                   , value
                   , values
                   , wheres
                   , (!)
                   , (.=.)
                   , (?)
                   )
import qualified Data.Text as T

import qualified Shioyaki.DbRecord as DbRecord
import qualified Shioyaki.Entity as Entity
import           Shioyaki.Env
import           Shioyaki.Monad
import           Shioyaki.Util.Sql
import qualified Shioyaki.Value as Value


insertMany :: ConnWrapper -> [Entity.Item] -> ShioyakiM ()
insertMany conn items =
  -- TODO: Move file to the collection directory as its registered name.
  -- TODO: Handle conflict of original name and registered name.
  void $ exec3 chunksInsert
    conn
    DbRecord.insertItem
    (map toDbRecord items)


toDbRecord :: Entity.Item -> DbRecord.Item
toDbRecord i =
  DbRecord.Item
    (T.unpack $ Entity.itemOriginalName i)
    (T.unpack $ Entity.itemRegisteredName i)
    (T.unpack $ Entity.itemExtension i)


selectAll :: MonadShioyaki t m => ConnWrapper -> t m [Value.CsTaggedItem]
selectAll c = selectBy c (const $ const $ return ())


selectByNames :: MonadShioyaki t m => ConnWrapper -> [String] -> t m [Value.CsTaggedItem]
selectByNames c originalOrRegisteredNames = selectBy c wh
  where
    wh i _t =
      wheres $ (i ! #originalName) `in'` values originalOrRegisteredNames
        `or'` (i ! #registeredName) `in'` values originalOrRegisteredNames


selectBy
  :: MonadShioyaki t m
  => ConnWrapper
  -> (Record Flat DbRecord.Item -> Record Flat (Maybe DbRecord.TagItem) -> QueryAggregate ())
  -> t m [Value.CsTaggedItem]
selectBy c wh = exec3 runRelation c rel ()
  where
    rel :: Relation () Value.CsTaggedItem
    rel = aggregateRelation $ do
      i <- query DbRecord.item
      t <- queryMaybe DbRecord.tagItem
      on $ just (i ! #originalName) .=. t ? #itemName
      wh i t
      g <- groupBy $
        DbRecord.Item
          |$| i ! #originalName
          |*| i ! #registeredName
          |*| i ! #extension
      return $
        Value.CsTaggedItem
          |$| g ! #originalName
          |*| g ! #registeredName
          |*| g ! #extension
          |*|
            ( Value.CsTag
                |$| groupConcatMaybe (t ? #tagName) (value Value.csTagSeparator)
            )
