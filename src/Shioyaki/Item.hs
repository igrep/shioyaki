module Shioyaki.Item
  ( registerFiles
  , listAll
  , findByNames
  , updateTags

  , replaceTags
  , addTags
  ) where


import           Data.List (sort, nub)
import qualified Data.Text as T
import           Data.Time.Format (formatTime, defaultTimeLocale)
import           Database.HDBC (ConnWrapper)
import           Lens.Micro (set, over)
import qualified System.FilePath as FilePath

import qualified Shioyaki.Entity as Entity
import           Shioyaki.Env
import qualified Shioyaki.Item.Dao as ItemDao
import           Shioyaki.Monad
import qualified Shioyaki.TagItem.Dao as TagItemDao
import qualified Shioyaki.Util.Db as Db
import qualified Shioyaki.Value as Value


registerFiles :: [FilePath] -> ShioyakiM ()
registerFiles paths =
  Db.withConnectionCommit $ \c ->
    ItemDao.insertMany c =<< mapM Shioyaki.Item.create paths



create :: MonadShioyaki t m => FilePath -> t m Entity.Item
create fpath = do
  t <- exec1 getModificationTime fpath
  return $
    Entity.newItem
      (T.pack $ FilePath.takeFileName fpath)
      (T.pack $ formatTime defaultTimeLocale "%Y/%m/%d/%H-%M-%S" t)
      (T.pack $ FilePath.takeExtension fpath)


listAll :: MonadShioyaki t m => ConnWrapper -> t m [Entity.Item]
listAll c =
  map Entity.itemFromCsTaggedItem <$> ItemDao.selectAll c


findByNames :: MonadShioyaki t m => ConnWrapper -> [String] -> t m [Entity.Item]
findByNames c ns =
  map Entity.itemFromCsTaggedItem <$> ItemDao.selectByNames c ns


updateTags :: ConnWrapper -> Value.CsTag -> [Entity.Item] -> ShioyakiM ()
updateTags c csTag items = TagItemDao.insertMany c records
  where
    records =
      Value.TagWithItem
        <$> Value.fromCsTag csTag
        <*> map (T.unpack . Entity.itemOriginalName) items


replaceTags :: [Value.Tag] -> Entity.Item -> Entity.Item
replaceTags = set Entity.itemTags


addTags :: [Value.Tag] -> Entity.Item -> Entity.Item
addTags newTags = over Entity.itemTags f
  where
    f oldTags = nub $ sort $ newTags ++ oldTags
