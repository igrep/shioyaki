module Shioyaki.Entity
  ( Item(Item, itemOriginalName, itemRegisteredName, itemExtension)
  , newItem
  , itemFromCsTaggedItem
  , itemTags
  ) where

import qualified Data.Text as T
import           Lens.Micro.TH (makeLenses)

import           Shioyaki.Value

data Item =
  Item
    { itemOriginalName :: T.Text
    , itemRegisteredName :: T.Text
    , itemExtension :: T.Text
    , _itemTags :: [Tag]
    } deriving (Eq, Show)

$(makeLenses ''Item)

newItem :: T.Text -> T.Text -> T.Text -> Item
newItem o r e = Item o r e []

itemFromCsTaggedItem :: CsTaggedItem -> Item
itemFromCsTaggedItem csti =
  Item
    (T.pack $ csTaggedItemOriginalName csti)
    (T.pack $ csTaggedItemRegisteredName csti)
    (T.pack $ csTaggedItemExtension csti)
    (tagsFromCsTag $ csTaggedItemTags csti)
