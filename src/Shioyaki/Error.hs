module Shioyaki.Error
  ( die
  ) where


import           Control.Monad.IO.Class (liftIO)
import qualified System.Exit as Exit

import           Shioyaki.Env (appName)
import           Shioyaki.Monad (ShioyakiM)


die :: String -> ShioyakiM a
die msg = liftIO $ Exit.die $ appName ++ ": ERROR: " ++ msg
