module Shioyaki.TagItem.Dao
  ( insertMany
  ) where


import           Control.Monad (void)
import           Data.Functor.ProductIsomorphic ((|$|), (|*|))
import           Database.HDBC (ConnWrapper)
import           Database.Relational (Pi, derivedInsert)

import qualified Shioyaki.DbRecord as DbRecord
import           Shioyaki.Env
import           Shioyaki.Monad
import qualified Shioyaki.Value as Value


insertMany :: ConnWrapper -> [Value.TagWithItem] -> ShioyakiM ()
insertMany conn =
  void . exec3 chunksInsert conn (derivedInsert p)
  where
    p :: Pi DbRecord.TagItem Value.TagWithItem
    p = Value.TagWithItem |$| #tagName |*| #itemName
