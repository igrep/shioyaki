module Shioyaki.Command
  ( main
  , envIoOfOptions
  , GlobalOptions(..)
  ) where

import           Database.HDBC (ConnWrapper(ConnWrapper))
import qualified Database.HDBC.Record as HDBC
import           Database.HDBC.Sqlite3 (connectSqlite3)
import qualified System.Directory as Dir

import           Shioyaki.Env
import qualified Shioyaki.Command.Register
import qualified Shioyaki.Command.Tag
import           Shioyaki.Monad
import           Shioyaki.Options
import qualified Shioyaki.Util.Db as Db


main :: IO ()
main = do
  gopts <- parseGlobalOptions
  let action =
        case globalOptionsCommand gopts of
            RegisterCommand args -> Shioyaki.Command.Register.main args
            TagCommand csTags itemNames -> Shioyaki.Command.Tag.main csTags itemNames
            _ -> error "Not implemented command"
  runShioyakiM
    (envIoOfOptions gopts)
    (Db.initializeSchemaWhenEmpty >> action)


envIoOfOptions :: GlobalOptions -> Env IO
envIoOfOptions gopts =
  Env
    { getModificationTime = Dir.getModificationTime
    , getConnection =
        ConnWrapper <$> connectSqlite3 (globalOptionsIndexPath gopts)
    , globalOptions = gopts
    , runRelation = \conn sel p -> do
        putStrLn $ "Running SQL: " ++ show sel
        putStrLn $ "With parameter: " ++ show p
        Db.runRelation conn sel p
    , chunksInsert = \conn ins xs -> do
        putStrLn $ "Running SQL: " ++ show ins
        putStrLn $ "To insert " ++ show xs
        HDBC.chunksInsert conn ins xs
    }
