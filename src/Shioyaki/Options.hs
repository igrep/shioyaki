module Shioyaki.Options
  ( GlobalOptions(..)
  , Command(..)
  , globalOptionsIndexPath
  , parseGlobalOptions
  ) where


import           Control.Applicative (some, (<**>))
import           Data.Semigroup ((<>))
import qualified Options.Applicative as Opt
import           System.FilePath ((</>))


data GlobalOptions =
  GlobalOptions
    { globalOptionsDbRoot :: FilePath
    , globalOptionsCommand :: Command
    } deriving (Eq, Show)

data Command =
  RegisterCommand [FilePath]
      -- ^ Add the files to the collection.
    | TagCommand String [String]
      -- ^ Add comma separated tag to the given item.
      -- TODO: --delete flag?
    | ListCommand
      -- ^ List all items or query with default query file
      --   (i.e. shioyaki query <path_to_default_query>).
    | QueryCommand FilePath
      -- ^ Query with HRR expression, using hint or runghc.
      --   Maybe adding --query option to 'TagCommand' and 'ListCommand' is better.
    deriving (Eq, Show)


parseGlobalOptions :: IO GlobalOptions
parseGlobalOptions = Opt.execParser globalOptionsInfo


globalOptionsInfo :: Opt.ParserInfo GlobalOptions
globalOptionsInfo =
  Opt.info
   (globalOptions <**> Opt.helper)
   (Opt.fullDesc <> Opt.progDesc "Manage my personal media DB" <> Opt.header "")


globalOptions :: Opt.Parser GlobalOptions
globalOptions =
  GlobalOptions
    <$>
      Opt.strOption
        ( Opt.long "db"
            <> Opt.short 'd'
            <> Opt.metavar "DB_ROOT_PATH"
            <> Opt.help "Path to collection database"
        )
    <*>
      Opt.subparser
        ( Opt.command "register"
            ( Opt.info
                (RegisterCommand <$> some (Opt.strArgument (Opt.metavar "FILES...")))
                Opt.briefDesc
            )
          <> Opt.command "tag"
            ( Opt.info
                ( TagCommand
                    <$> Opt.strArgument (Opt.metavar "TAG_NAME")
                    <*> some (Opt.strArgument (Opt.metavar "ITEM_NAME..."))
                )
                Opt.briefDesc
            )
          <> Opt.command "list"
            (Opt.info (pure ListCommand) Opt.briefDesc)
          <> Opt.command "query"
            ( Opt.info
                ( QueryCommand
                    <$> Opt.strArgument (Opt.metavar "QUERY_FILE_NAME")
                )
                Opt.briefDesc
            )
        )


globalOptionsIndexPath :: GlobalOptions -> FilePath
globalOptionsIndexPath = (</> "index.db") . globalOptionsDbRoot
