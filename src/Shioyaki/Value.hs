module Shioyaki.Value
  ( Tag
  , tagsFromCsTag

  , CsTaggedItem(..)
  , CsTag(CsTag)
  , toCsTag
  , fromCsTag
  , csTagSeparator

  , TagWithItem(..)
  ) where


import           Data.List (intercalate)
import           Data.List.Split (splitOn)
import           Data.String (IsString, fromString)
import qualified Data.Text as T
import           Database.HDBC (SqlValue)
import           Database.Record (FromSql, ToSql)
import           GHC.Generics (Generic)

import           Shioyaki.Util.Db


type Tag = T.Text


-- FIXME: makeRelationalRecordDefault-ed type must not be newtype but data.
-- | Comma Separated Tag. Represents a group of tag names input by user,
--   or saved in a DB record.
newtype CsTag =
  CsTag { asRawMaybeText :: Maybe String } deriving (Eq, Show, Generic)

instance FromSql SqlValue CsTag

$(makeRelationalRecord ''CsTag)

instance IsString CsTag where
  fromString = CsTag . Just


-- | Item with 'CSTag'
data CsTaggedItem =
  CsTaggedItem
    { csTaggedItemOriginalName :: String
    , csTaggedItemRegisteredName :: String
    , csTaggedItemExtension :: String
    , csTaggedItemTags :: CsTag
    } deriving (Eq, Show, Generic)

instance FromSql SqlValue CsTaggedItem

$(makeRelationalRecord ''CsTaggedItem)


data TagWithItem =
  TagWithItem
    { tagWithItemTagName :: String
    , tagWithItemItemOriginalName :: String
    } deriving (Eq, Show, Generic)

$(makeRelationalRecord ''TagWithItem)

instance ToSql SqlValue TagWithItem

toCsTag :: [String] -> CsTag
toCsTag = fromString . intercalate csTagSeparator


fromCsTag :: CsTag -> [String]
fromCsTag = maybe [] (splitOn csTagSeparator) . asRawMaybeText


csTagSeparator :: String
csTagSeparator = ","


tagsFromCsTag :: CsTag -> [Tag]
tagsFromCsTag = map T.pack . fromCsTag
