module Shioyaki.Util.Db
  ( defineTable
  , makeRelationalRecord
  , withConnection
  , withConnectionCommit
  , initializeSchemaWhenEmpty
  , runRelation
  ) where


import           Control.Monad (unless)
import           Control.Monad.IO.Class (liftIO)
import           Database.HDBC
                   ( IConnection
                   , ConnWrapper(..)
                   , commit
                   , runRaw
                   , SqlValue
                   )
import           Database.HDBC.Query.TH (defineTableFromDB)
import           Database.HDBC.Record (runQuery')
import           Database.HDBC.Schema.Driver (typeMap, driverConfig)
import           Database.HDBC.Schema.SQLite3 (driverSQLite3)
import           Database.HDBC.Session (withConnectionIO')
import           Database.HDBC.Sqlite3 (connectSqlite3)
import           Database.Record (ToSql, FromSql)
import           Database.Relational
                   ( Relation
                   , relationalQuery

                   , Config(..)
                   , NameConfig(..)
                   , SchemaNameMode(SchemaNotQualified)
                   , defaultConfig
                   )
import           Database.Relational.TH (makeRelationalRecordDefault')
import           Database.Record.TH
                    ( defaultNameConfig
                    , columnName
                    )
import           GHC.Generics (Generic)
import           Language.Haskell.TH.Name.CamelCase (varCamelcaseName)
import           Language.Haskell.TH (Q, Dec, Name)
import           System.Directory (doesDirectoryExist)

import           Shioyaki.Env (getConnection, getIndexPath)
import           Shioyaki.Monad

import qualified Paths_shioyaki


withConnection :: (ConnWrapper -> ShioyakiM a) -> ShioyakiM a
withConnection action = do
  e <- askEnv
  liftIO $ withConnectionIO'
    (getConnection e)
    (runShioyakiM e . action)


withConnectionCommit :: (ConnWrapper -> ShioyakiM a) -> ShioyakiM a
withConnectionCommit action =
  withConnection $ \conn -> do
    r <- action conn
    liftIO $ commit conn
    return r


defineTable :: String -> Q [Dec]
defineTable tableName =
  defineTableFromDB
    (connectSqlite3 "db/empty.db")
    ( driverSQLite3
        { typeMap =
            [ ("FLOAT", [t|Double|])
            , ("INTEGER", [t|Int|])
            ]
        , driverConfig = relationalQueryThConfig
        }
    ) -- overwrite the default type map with yours
    "main" -- schema name, ignored by SQLite
    tableName
    [''Eq, ''Show, ''Generic]


makeRelationalRecord :: Name -> Q [Dec]
makeRelationalRecord = makeRelationalRecordDefault' relationalQueryThConfig


relationalQueryThConfig :: Config
relationalQueryThConfig =
  defaultConfig {
    nameConfig = NameConfig
      { recordConfig =
          defaultNameConfig {
            columnName = \table column ->
              varCamelcaseName $ table ++ "_" ++ column
          }
      , relationVarName = relationVarName $ nameConfig defaultConfig
      }
  , disableSpecializedProjection = True
  , verboseAsCompilerWarning     = False
  , schemaNameMode               = SchemaNotQualified
  }


initializeSchemaWhenEmpty :: ShioyakiM ()
initializeSchemaWhenEmpty = do
  dbPath <- askEnvs getIndexPath
  es <- liftIO $ doesDirectoryExist dbPath
  unless es $
    withConnectionCommit $ \conn ->
      liftIO $ do
        ddl <- readFile =<< Paths_shioyaki.getDataFileName "db/schema.sql"
        runRaw conn ddl
        commit conn


runRelation
  :: (ToSql SqlValue p, IConnection conn, FromSql SqlValue a)
  => conn
  -> Relation p a
  -> p -> IO [a]
runRelation conn q = runQuery' conn (relationalQuery q)
