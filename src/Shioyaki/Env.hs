module Shioyaki.Env
  ( Env(..)
  , getIndexPath
  , appName
  ) where


import           Data.Time.Clock (UTCTime)
import           Database.HDBC (ConnWrapper, SqlValue)
import           Database.Record (ToSql, FromSql)
import           Database.Relational (Relation, Insert)

import           Shioyaki.Options


data Env m =
  Env
    { getModificationTime :: FilePath -> m UTCTime
    , getConnection :: m ConnWrapper
    , globalOptions :: GlobalOptions
    , runRelation
        :: forall p a. (ToSql SqlValue p, FromSql SqlValue a, Show p)
        => ConnWrapper -> Relation p a -> p -> m [a]
    , chunksInsert
        :: forall a. (ToSql SqlValue a, Show a)
        => ConnWrapper -> Insert a -> [a] -> m [[Integer]]
    }


getIndexPath :: Env m -> FilePath
getIndexPath = globalOptionsIndexPath . globalOptions


appName :: String
appName = "shioyaki"
