module Shioyaki.Monad
  ( ShioyakiM
  , MonadShioyaki
  , runShioyakiM
  , askEnv
  , askEnvs
  , exec0
  , exec1
  , exec3
  ) where


import           Control.Monad.Trans.Class (MonadTrans)
import           Control.Monad.Reader (MonadReader, ask, asks, lift, runReaderT, ReaderT)

import           Shioyaki.Env


type ShioyakiM a = ReaderT (Env IO) IO a

type MonadShioyaki t m =
  (MonadTrans t, Monad m, MonadReader (Env m) (t m))


runShioyakiM :: Env IO -> ShioyakiM a -> IO a
runShioyakiM = flip runReaderT


askEnv :: MonadReader (Env n) m => m (Env n)
askEnv = ask


askEnvs :: MonadShioyaki t m => (Env m -> a) -> t m a
askEnvs = asks


exec0 :: MonadShioyaki t m => (Env m -> m a) -> t m a
exec0 act = do
  e <- ask
  lift $ act e


exec1 :: MonadShioyaki t m => (Env m -> a -> m b) -> a -> t m b
exec1 f a = exec0 (f <*> pure a)


exec3 :: MonadShioyaki t m => (Env m -> a -> b -> c -> m d) -> a -> b -> c -> t m d
exec3 f a b c = exec0 (f <*> pure a <*> pure b <*> pure c)
