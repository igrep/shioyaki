CREATE TABLE IF NOT EXISTS item (
  original_name text primary key not null,
  registered_name text not null,
  extension text not null
);

CREATE TABLE IF NOT EXISTS tag (
  tag_name text primary key not null
);

CREATE TABLE IF NOT EXISTS tag_item (
  id integer primary key not null,
  tag_name text references tag(tag_name) on update cascade on delete cascade not null,
  item_name text references tag(item_name) on update cascade on delete cascade not null
);
